package LBGMIPortal.Automation;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class initBrowser {
	private static WebDriver driver;
	public static configReader config = new configReader();
	//public static String downloadFilepath = "C:\\Users\\quangphamm\\Downloads\\";
	public static String downloadFilepath = config.getDownloadFilepath();
	
	@SuppressWarnings("deprecation")
	public static WebDriver launchBrowsers() {
//		System.setProperty("webdriver.chrome.driver",
//			"D:\\quangphamm\\eclipse-workspace\\Automation\\chromedriver_win32\\chromedriver.exe");
		
		System.setProperty("webdriver.chrome.driver", config.getChromePath());
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.default_directory", downloadFilepath);
		ChromeOptions options = new ChromeOptions();
		HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
		options.setExperimentalOption("prefs", chromePrefs);
		options.addArguments("--test-type");
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		cap.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
		cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		cap.setCapability(ChromeOptions.CAPABILITY, options);
		driver = new ChromeDriver(cap);
		//driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		return driver;
	}
}

package LBGMIPortal.Automation;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class configReader {

	Properties pro = new Properties();;

	public configReader() {
		try {
			File source = new File(".\\configuration\\configproperties\\configPropertiesFile.txt");
			FileInputStream input = new FileInputStream(source);

			pro.load(input);

		} catch (Exception exp) {

			System.out.println("Exception is: ---" + exp.getMessage());
		}
	}

	public String getChromePath() {
		return pro.getProperty("ChromeDriver");
	}

	public String getExtentHtmlReporterPath() {
		return pro.getProperty("ExtentHtmlReporterPath");
	}

	public String getDownloadFilepath() {
		return pro.getProperty("downloadFilepath");
	}

}

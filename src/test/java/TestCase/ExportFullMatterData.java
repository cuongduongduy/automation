package TestCase;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import LBGMIPortal.Automation.configReader;
import LBGMIPortal.Automation.initBrowser;
import module.SignInFlow;
import module.downloadFile;
import module.exportData;
import storable.WebSiteURLList;

public class ExportFullMatterData{
	private static WebDriver driver;
	private static SignInFlow pageSignIn;
	private static exportData export;
	
	private static ExtentHtmlReporter htmlReporter;
	private static ExtentReports extent;
	private static ExtentTest logger;
	
	private static String Email = "LBGClient@nghiadinhtrongevershedssuth.onmicrosoft.com";
	private static String Password = "Wildbouy123";
	static configReader config = new configReader();

	@BeforeClass
	public void setup() throws InterruptedException {
		driver = initBrowser.launchBrowsers();
		pageSignIn = new SignInFlow(driver);
		export = new exportData(driver);
		htmlReporter = new ExtentHtmlReporter(config.getExtentHtmlReporterPath()+"ExportMatterList.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		driver.navigate().to(WebSiteURLList.Overview.URL());
		pageSignIn.doLogin(Email, Password);
	}
	
	@Test(priority = 1)
	public  void verifyButtonExportMatterList() throws InterruptedException {
		logger = extent.createTest("Verify Button Export Matter List");
		try {		
			logger.info("Verify Export Data tool tip");
			Assert.assertTrue(export.verifyTooltipExport());
			logger.pass("Tooltip is corrected as: " +export.btnExport().getAttribute("data-original-title"));

		} catch (Exception e) {
			logger.fail("Testcase 'verifyButtonExportMatterList' is FAILED: " + e.getMessage());
			Assert.fail("Testcase 'verifyButtonExportMatterList' is FAILED: " + e.getMessage());
		}
	}
	
	@Test(priority = 2)
	public void verifyExportedFileName() throws InterruptedException{
		logger = extent.createTest("Verify Exported File's Name");

		
		try {		
			logger.info("Verify Export file name");
			Assert.assertTrue(downloadFile.VerifyExpectedFileName(driver));
			logger.pass("file name is correct: "+downloadFile.fileName);

		} catch (Exception e) {
			logger.fail("Testcase 'verifyExportedFileName' is FAILED: " + e.getMessage());
			Assert.fail("Testcase 'verifyExportedFileName' is FAILED: " + e.getMessage());
		}
	}
	
	@AfterClass
	public void closeWebSite() {
		extent.flush();
		driver.quit();
	}

}

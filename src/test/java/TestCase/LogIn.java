package TestCase;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import LBGMIPortal.Automation.configReader;
import LBGMIPortal.Automation.initBrowser;
import module.SignInFlow;
import module.pageHeader;
import storable.TestAccountList;
import storable.WebSiteURLList;
import module.pageNAV;
import pageObjects.OverviewPage;

public class LogIn {
	private WebDriver driver;
	private SignInFlow pageSignIn;
	private pageHeader pageHeader;
	private pageNAV pageNAV;
	private OverviewPage overview;
	
	private static ExtentHtmlReporter htmlReporter;
	private static ExtentReports extent;
	private static ExtentTest logger;
	private TestAccountList account = new TestAccountList(
			"LBGClient@nghiadinhtrongevershedssuth.onmicrosoft.com",
			"Wildbouy123", 
			"LBGClient User"
			);
	configReader config = new configReader();
	
	@BeforeClass
	public void setup() {
		driver = initBrowser.launchBrowsers();

		pageSignIn = new SignInFlow(driver);
		pageHeader = new pageHeader(driver);
		pageNAV = new pageNAV(driver);
		overview = new OverviewPage(driver);
		
		htmlReporter = new ExtentHtmlReporter(config.getExtentHtmlReporterPath()+"Login.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
	}

	@Test(priority = 1)
	public void doLogin() {
		logger = extent.createTest("Verify Login (valid)");
		try {
			logger.info("Access page WebSite");
			driver.navigate().to(WebSiteURLList.Overview.URL());
			
			logger.info("Login with account "+account.getAccountName()+": " +account.getEmail());
			pageSignIn.doLogin(account.getEmail(), account.getPassword());
			logger.pass("Logged in Successfully");
			
			logger.info("Verify User 's info");
			Assert.assertTrue(pageNAV.userInfo_Name().getText().equalsIgnoreCase(account.getAccountName()));
			logger.pass("User 's info is corrected as: " + pageNAV.userInfo_Name().getText());

		} catch (Exception e) {
			logger.fail("Testcase 'doLogin' is FAILED: " + e.getMessage());
			Assert.fail("Testcase 'doLogin' is FAILED: " + e.getMessage());
		}
	}
	
	@Test(priority = 2)
	public void verifyDefaultPage() {
		logger = extent.createTest("Verify Default Page after Logging");
		try {
			logger.info("verify Website Header's title");
			Assert.assertTrue(pageHeader.verifyPageTitle());
			logger.pass("Page title is corrected as: " + pageHeader.pageTitle());
			
			logger.info("verify Default Page's URL");
			Assert.assertTrue(overview.verifyPageURL());
			logger.pass("Page URL is corrected: " + driver.getCurrentUrl());

		} catch (Exception e) {
			logger.fail("Testcase 'Login' is FAILED: " + e.getMessage());
			Assert.fail("Testcase 'Login' is FAILED: " + e.getMessage());
		}
	}

	@AfterClass
	public void finish() {
		extent.flush();
		driver.quit();
	}
}

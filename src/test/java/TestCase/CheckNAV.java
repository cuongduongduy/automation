package TestCase;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import LBGMIPortal.Automation.configReader;
import LBGMIPortal.Automation.initBrowser;
import module.SignInFlow;
import module.pageNAV;
import pageObjects.NavigationMenu;
import storable.WebSiteURLList;

public class CheckNAV {

	private static WebDriver driver;

	static configReader config = new configReader();

	private ExtentHtmlReporter htmlReporter;
	private ExtentReports extent;
	private ExtentTest logger;

	private String Email = "LBGClient@nghiadinhtrongevershedssuth.onmicrosoft.com";
	private String Password = "Wildbouy123";

	private SignInFlow pageSignIn;
	private NavigationMenu NAVMenu;
	private pageNAV pageNAV;

	@BeforeClass
	public void setup() throws InterruptedException {
		driver = initBrowser.launchBrowsers();

		htmlReporter = new ExtentHtmlReporter(config.getExtentHtmlReporterPath() + "NavigationMenu.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

		pageSignIn = new SignInFlow(driver);
		NAVMenu = new NavigationMenu(driver);
		pageNAV = new pageNAV(driver);

		driver.navigate().to(WebSiteURLList.Overview.URL());
		pageSignIn.doLogin(Email, Password);
	}

	@Test(priority = 1)
	public void verifyNAV_MainItem() {
		logger = extent.createTest("Navigation - Main Item on Menu");
		try {

			logger.info("Verify NAV Menu item");
			Assert.assertTrue(NAVMenu.verifyMenuItem_Main());
			logger.pass("List menu on NAV is corrected");

		} catch (Exception e) {
			logger.fail("Testcase 'verifyNAV_MainItem' is FAILED: " + e.getMessage());
			Assert.fail("Testcase 'verifyNAV_MainItem' is FAILED: " + e.getMessage());
		}
	}

	@Test(priority = 2)
	public void verifyNAV_SubMenu() {
		logger = extent.createTest("Navigation - Sub-Item on Menu");
		try {
			Thread.sleep(5000);

			logger.info("Verify child Menu items and its URL of Financial");
			Assert.assertTrue(NAVMenu.verifyMenuItem_Child("Financial"));
			logger.pass("List child menus and its URL of Financial on NAV are corrected ");

			logger.info("Verify child Menu items  and its URLof Real Estate");
			Assert.assertTrue(NAVMenu.verifyMenuItem_Child("Real Estate"));
			logger.pass("List child menus  and its URL of Real Estate on NAV are corrected ");

			logger.info("Verify child Menu items and its URL of Litigation");
			Assert.assertTrue(NAVMenu.verifyMenuItem_Child("Litigation"));
			logger.pass("List child menus and its URL of Litigation on NAV are corrected ");

		} catch (Exception e) {
			logger.fail("Testcase 'verifyNAV_SubMenu' is FAILED: " + e.getMessage());
			Assert.fail("Testcase 'verifyNAV_SubMenu' is FAILED: " + e.getMessage());
		}
	}
	
	@Test(priority = 3)
	public void verifyLogo() throws Exception {
		logger = extent.createTest("Navigation - Site logo check");
		try {
			
			logger.info("Verify Site Logo");
			Assert.assertTrue(pageNAV.CheckImage());
			logger.pass("Site logo is displayed!");
			
			logger.info("Verify Site Logo name package");
			Assert.assertTrue(pageNAV.verifySiteLogoPackage());
			logger.pass("Site Logo name package is corrected: "+pageNAV.siteLogo().getAttribute("src"));
			
		} catch (Exception e) {
			logger.fail("Testcase 'verifyLogo' is FAILED: " + e.getMessage());
			Assert.fail("Testcase 'verifyLogo' is FAILED: " + e.getMessage());
		}
	}

	@AfterClass
	public void finish() {
		extent.flush();
		driver.quit();
	}

}

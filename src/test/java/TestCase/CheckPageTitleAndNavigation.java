package TestCase;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import LBGMIPortal.Automation.configReader;
import LBGMIPortal.Automation.initBrowser;
import module.MainPageTitle;
import module.SignInFlow;
import pageObjects.Financial_Dashboard;
import pageObjects.OverviewPage;
import pageObjects.RealEstate_Dashboard;
import storable.WebSiteURLList;

public class CheckPageTitleAndNavigation {

	private static WebDriver driver;

	static configReader config = new configReader();

	private ExtentHtmlReporter htmlReporter;
	private ExtentReports extent;
	private ExtentTest logger;

	private String Email = "LBGClient@nghiadinhtrongevershedssuth.onmicrosoft.com";
	private String Password = "Wildbouy123";

	private SignInFlow pageSignIn;
	private OverviewPage overview;
	private Financial_Dashboard financial;
	private RealEstate_Dashboard RealEstate;
	private MainPageTitle pageTitle;

	@BeforeClass
	public void setup() throws InterruptedException {
		driver = initBrowser.launchBrowsers();

		htmlReporter = new ExtentHtmlReporter(config.getExtentHtmlReporterPath() + "CheckPageTitleAndNavigation.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

		pageSignIn = new SignInFlow(driver);
		overview = new OverviewPage(driver);
		financial = new Financial_Dashboard(driver);
		RealEstate = new RealEstate_Dashboard(driver);
		pageTitle = new MainPageTitle(driver);

		driver.navigate().to(WebSiteURLList.Overview.URL());
		pageSignIn.doLogin(Email, Password);
	}

	@Test(priority = 1)
	public void verifyPageTileAndURL() {
		logger = extent.createTest("Verify Overview page");

		try {
			logger.info("verify Overview Page's URL");
			Assert.assertTrue(overview.verifyPageURL());
			logger.pass("Page URL is corrected: " + driver.getCurrentUrl());

			logger.info("verify Overview Page's Title");
			System.out.println(overview.getOverviewPageTitle());
			Assert.assertTrue(overview.verifyPageTitle());
			logger.pass("Page's Title is corrected: " + overview.getOverviewPageTitle());
			
		} catch (Exception e) {
			logger.fail("Testcase 'verifyPageTileAndURL' is FAILED: " + e.getMessage());
			Assert.fail("Testcase 'verifyPageTileAndURL' is FAILED: " + e.getMessage());
		}
	}
	
	@Test(priority = 2)
	public void verifyFinancialPage() {
		logger = extent.createTest("Verify Financial page");

		try {
			logger.info("Access Finalcial Page (Dashboard)");
			financial.accessPage();
			
			logger.info("verify Financial Page's URL");
			Assert.assertTrue(financial.verifyPageURL());
			logger.pass("Page URL is corrected: " + driver.getCurrentUrl());

			logger.info("verify Financial Page's Title");
			System.out.println(pageTitle.getPageTitle());
			Assert.assertTrue(financial.verifyPageTitle());
			logger.pass("Page's Title is corrected: " + pageTitle.getPageTitle());
			
		} catch (Exception e) {
			logger.fail("Testcase 'verifyFinancialPage' is FAILED: " + e.getMessage());
			Assert.fail("Testcase 'verifyFinancialPage' is FAILED: " + e.getMessage());
		}
	}
	
	@Test(priority = 3)
	public void verifyRealEstatePage() {
		logger = extent.createTest("Verify Real Estate page");

		try {
			logger.info("Access Real Estate Page (Dashboard)");
			RealEstate.accessPage();
			
			logger.info("verify Real Estate Page's URL");
			Assert.assertTrue(RealEstate.verifyPageURL());
			logger.pass("Page URL is corrected: " + driver.getCurrentUrl());

			logger.info("verify Real Estate Page's Title");
			System.out.println(pageTitle.getPageTitle());
			Assert.assertTrue(RealEstate.verifyPageTitle());
			logger.pass("Page's Title is corrected: " + pageTitle.getPageTitle());
			
		} catch (Exception e) {
			logger.fail("Testcase 'verifyRealEstatePage' is FAILED: " + e.getMessage());
			Assert.fail("Testcase 'verifyRealEstatePage' is FAILED: " + e.getMessage());
		}
	}
	
	@AfterClass
	public void finish() {
		extent.flush();
		driver.quit();
	}
}

package storable;

public enum WebSiteURLList {
	Overview("https://lbgmiqc.azurewebsites.net"),
	Financial("https://lbgmiqc.azurewebsites.net/Financial"),
	RealEstate("https://lbgmiqc.azurewebsites.net/RealEstate"),
	Litigation("https://lbgmiqc.azurewebsites.net/Litigation");
	
	private String url;

	WebSiteURLList(String url) {
		this.url = url;
	}

	public String URL() {
		return url;
	}
}

package storable;

public class MenuURL {
	private String menu;
	private String url;

	public String MenuName() {
		return menu;
	}

	public String Url() {
		return url;
	}

	public MenuURL(String menu, String url) {
		this.menu = menu;
		this.url = url;
	}

	public MenuURL() {
	}

}

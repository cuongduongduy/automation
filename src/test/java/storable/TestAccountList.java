package storable;

public class TestAccountList {
	private String email;
	private String password;
	private String accountName;

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public String getAccountName() {
		return accountName;
	}

	public TestAccountList(String email, String password, String accountName) {
		this.email = email;
		this.password = password;
		this.accountName = accountName;
	}

}

package pageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import module.pageNAV;
import storable.MenuURL;
import storable.WebSiteURLList;

public class NavigationMenu {
	private WebDriver driver;
	private pageNAV pageNAV;
	private String baseFinancialURL = WebSiteURLList.Financial.URL();
	private String baseRealEstateURL = WebSiteURLList.RealEstate.URL();
	private String baseLitigationURL = WebSiteURLList.Litigation.URL();

	public NavigationMenu(WebDriver driver) {
		this.driver = driver;
		pageNAV = new pageNAV(driver);
	}

	public boolean verifyMenuItem_Main() {
		boolean verify = true;
		List<WebElement> menuItem = pageNAV.menuItem_Parent();
		String[] expectedList = { "Overview", "Financial", "Real Estate", "Litigation" };
		if (menuItem.size() < expectedList.length) {
			verify = false;
		}
		for (int i = 0; i < expectedList.length; i++) {
			if (!menuItem.get(i).getText().equalsIgnoreCase(expectedList[i])) {
				verify = false;
				break;
			}
		}
		return verify;
	}

	public boolean verifyMenuItem_Child(String parentMenu) throws InterruptedException {
		boolean verify = true;
		MenuURL[] expectedArraysMenuURL = new MenuURL[] {};
		try {
			driver.findElement(By.xpath(
					"//li[contains(@class, 'treeview')]/a/span[contains(text(),'" + parentMenu + "')]/../span[2]"))
					.click();

			Thread.sleep(5000);
			List<WebElement> menuItem = pageNAV.menuItem_Child();

			switch (parentMenu) {
			case "Financial":
				expectedArraysMenuURL = new MenuURL[] { 
						new MenuURL("Dashboard", baseFinancialURL),
						new MenuURL("REL", baseFinancialURL + "/REL"),
						new MenuURL("REPG", baseFinancialURL + "/REPG") };
				break;
			case "Real Estate":
				expectedArraysMenuURL = new MenuURL[] { 
						new MenuURL("Dashboard", baseRealEstateURL),
						new MenuURL("Acquisitions", baseRealEstateURL + "/Acquisitions"),
						new MenuURL("Disposals", baseRealEstateURL + "/Disposals"),
						new MenuURL("Estate Management", baseRealEstateURL + "/EstateManagement"),
						new MenuURL("Lease Renewals", baseRealEstateURL + "/LeaseRenewals"),
						new MenuURL("Tenancy", baseRealEstateURL + "/Tenancy") };
				break;
			case "Litigation":
				expectedArraysMenuURL = new MenuURL[] { 
						new MenuURL("Dashboard", baseLitigationURL),
						new MenuURL("Determinations", baseLitigationURL + "/Determinations"),
						new MenuURL("Dilapidations", baseLitigationURL + "/Dilapidations"),
						new MenuURL("Estate Management", baseLitigationURL + "/EstateManagement"),
						new MenuURL("Lease Renewals", baseLitigationURL + "/LeaseRenewals"),
						new MenuURL("Litigation", baseLitigationURL + "/Litigation"),
						new MenuURL("Tenancy", baseLitigationURL + "/Tenancy") };
				break;
			}

			if (menuItem.size() < expectedArraysMenuURL.length) {
				verify = false;
			}
			for (int i = 0; i < expectedArraysMenuURL.length; i++) {
				if (!menuItem.get(i).getText().equalsIgnoreCase(expectedArraysMenuURL[i].MenuName())) {
					verify = false;
					break;
				} else if (!verifyURLOfMenuItem(menuItem.get(i).getText(), expectedArraysMenuURL[i].Url())) {
					verify = false;
					break;
				}
			}
		} catch (Exception e) {
			System.out.println(
					"Only 3 values 'Litigate, Real Estate, Financial' are accepted. Current values is: " + parentMenu);
			verify = false;
		}
		return verify;
	}

	public boolean verifyURLOfMenuItem(String itemName, String expectedURL) {
		boolean verify = true;
		WebElement itemElement = pageNAV.itemURL(itemName);
		System.out.println(itemElement.getAttribute("href"));

		if (!itemElement.getAttribute("href").equals(expectedURL)) {
			verify = false;
		}
		return verify;
	}
}

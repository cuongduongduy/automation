package pageObjects;

import org.openqa.selenium.WebDriver;

import module.MainPageTitle;
import storable.WebSiteURLList;

public class RealEstate_Dashboard {

	private WebDriver driver;
	MainPageTitle pageTitle;
	
	private String pageURL = WebSiteURLList.RealEstate.URL();
	private String expectedTitle = "Real Estate Intelligence";
	
	public RealEstate_Dashboard(WebDriver driver) {
		this.driver = driver;
	}
	
	public boolean verifyPageTitle() {
		pageTitle = new MainPageTitle(driver);
		boolean verify = true;
		if(!pageTitle.getPageTitle().equalsIgnoreCase(expectedTitle)) {
			verify = false;
		}
		return verify;
	}
	
	public boolean verifyPageURL() {
		boolean verify = true;
		if(!driver.getCurrentUrl().equalsIgnoreCase(pageURL)) {
			verify = false;
		}
		return verify;
	}
	
	public void accessPage() throws InterruptedException {
		driver.navigate().to(pageURL);
	}

}

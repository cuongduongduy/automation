package pageObjects;

import org.openqa.selenium.WebDriver;

import module.MainPageTitle;
import storable.WebSiteURLList;

public class Financial_Dashboard {
	private WebDriver driver;
	MainPageTitle pageTitle;
	
	private String pageURL = WebSiteURLList.Financial.URL();
	private String expectedTitle = "Financial Intelligence";
	public Financial_Dashboard(WebDriver driver) {
		this.driver = driver;
	}
	
	public boolean verifyPageTitle() {
		pageTitle = new MainPageTitle(driver);
		boolean verify = true;
		if(!pageTitle.getPageTitle().equalsIgnoreCase(expectedTitle)) {
			verify = false;
		}
		return verify;
	}
	
	public boolean verifyPageURL() {
		boolean verify = true;
		if(!driver.getCurrentUrl().equalsIgnoreCase(pageURL)) {
			verify = false;
		}
		return verify;
	}
	
	public void accessPage() throws InterruptedException {
		driver.navigate().to(pageURL);
	}
	
}

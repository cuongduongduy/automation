package pageObjects;

import org.openqa.selenium.WebDriver;

import storable.WebSiteURLList;
import module.MainPageTitle;

public class OverviewPage {
	private WebDriver driver;
	MainPageTitle pageTitle;

	private String pageURL = WebSiteURLList.Overview.URL();
	
	private String expectedTitle = "Client Intelligence";
	private int count_expectedTitle =  expectedTitle.length();
	

	public OverviewPage(WebDriver driver) {
		this.driver = driver;
	}

	public String getOverviewPageTitle() {
		pageTitle = new MainPageTitle(driver);
		return pageTitle.getPageTitle().substring(0, count_expectedTitle);
	}
		
	public boolean verifyPageTitle() {
		boolean verify = true;
		if(!getOverviewPageTitle().equalsIgnoreCase(expectedTitle)) {
			verify = false;
		}
		return verify;
	}
	
	public boolean verifyPageURL() {
		boolean verify = true;
		if(!driver.getCurrentUrl().equalsIgnoreCase(pageURL+"/")) {
			verify = false;
		}
		return verify;
	}
}

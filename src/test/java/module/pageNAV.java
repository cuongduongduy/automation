package module;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import storable.WebSiteURLList;

public class pageNAV {
	private WebDriver driver;
	private elementVisible elementVisible;

	private WebElement ImageFile;
	private By by_siteLogo = By.cssSelector("#sidebar-logo img");

	public pageNAV(WebDriver driver) {
		this.driver = driver;
		elementVisible = new elementVisible(driver);
	}

	public WebElement siteLogo() {
		ImageFile = elementVisible.isElementDisplay(driver.findElement(by_siteLogo));
		return ImageFile;
	}

	public WebElement siteUserPanel() {
		return elementVisible.isElementDisplay(driver.findElement(By.cssSelector(".sidebar-user-panel")));	
	}

	public WebElement userInfo_Name() {
		return elementVisible.isElementDisplay(driver.findElement(By.cssSelector(".name")));
	}

	public List<WebElement> menuItem_Parent() {
		return driver.findElements(By.xpath("//li[contains(@class, 'treeview')]/a/span[1]"));
	}

	public List<WebElement> menuItem_Child() throws InterruptedException {
		return driver.findElements(By.cssSelector(".treeview-menu.menu-open a span"));
	}

	public WebElement itemURL(String itemName) {
		return driver.findElement(
				By.xpath("//ul[@class='treeview-menu menu-open']//span[contains(text(), '" + itemName + "')]/.."));
	}

	public boolean CheckImage() throws Exception {
		Thread.sleep(10000);
		ImageFile = siteLogo();
		boolean ImagePresent = (boolean) ((JavascriptExecutor) driver).executeScript(
				"return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0",
				ImageFile);
		if (!ImagePresent) {
			System.out.println("Image not displayed.");		
		} else {
			System.out.println("Image displayed.");
		}
		return ImagePresent;
	}
	
	public boolean verifySiteLogoPackage() {
		boolean verify = true;
		ImageFile = siteLogo();
		if(!ImageFile.getAttribute("src").contains(WebSiteURLList.Overview.URL()+"/Content/images/Lloyds%20Logo.png")) {
			verify = false;
		}
		return verify;
	}

}

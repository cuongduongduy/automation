package module;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import module.elementVisible;

public class SignInFlow {
	private WebDriver driver;
	private WebElement txtEmail;
	private WebElement btnSumit;
	private WebElement txtPassword;
	private WebElement btnBack;
	private WebElement btnOtherAcc;
	private WebElement frmTitle;
	private elementVisible elementVisible;

	public SignInFlow(WebDriver driver) {
		this.driver = driver;
		elementVisible = new elementVisible(driver);
	}

	// Element list
	public WebElement LoginHeader() {
		return driver.findElement(By.id("loginHeader"));
	}

	public WebElement TextBoxEmail() {
		txtEmail = elementVisible.isElementDisplay(driver.findElement(By.cssSelector("input[name='loginfmt']")));
		return txtEmail;
	}

	public WebElement SubmitButton() {
		btnSumit = elementVisible.isElementClickAble(driver.findElement(By.id("idSIButton9")));
		return btnSumit;
	}

	public WebElement passwordTextbox() {
		txtPassword = elementVisible.isElementDisplay(driver.findElement(By.cssSelector("input[type='password']")));
		return txtPassword;
	}

	public WebElement BackButton() {
		btnBack = elementVisible.isElementClickAble(driver.findElement(By.id("idBtn_Back")));
		return btnBack;
	}

	public WebElement pickAnotherAccount() {
		btnOtherAcc = elementVisible.isElementClickAble(driver.findElement(By.id("otherTileText")));
		return btnOtherAcc;
	}

	// Functional list
	public boolean isDisplay_SignInTitle() {
		return LoginHeader().isDisplayed();
	}

	public String getFormTitle() {
		frmTitle = elementVisible.isElementDisplay(driver.findElement(By.cssSelector(".text-title")));
		return frmTitle.getText();
	}

	public void inputTxt_Email(String Email) {
		TextBoxEmail().clear();
		TextBoxEmail().sendKeys(Email);
	}

	public void inputTxt_Pass(String Password) {
		passwordTextbox().clear();
		passwordTextbox().sendKeys(Password);
	}

	public String click_SubmitBtn() {
		String btnText = SubmitButton().getAttribute("value");
		SubmitButton().click();
		return btnText;
	}

	public void click_BackBtn() {
		BackButton().click();
	}

	public void doLogin(String Email, String Password) throws InterruptedException {
		if (getFormTitle().equalsIgnoreCase("Pick an account")) {
			pickAnotherAccount().click();
		}

		inputTxt_Email(Email);

		click_SubmitBtn();
		Thread.sleep(3000);
		inputTxt_Pass(Password);
		click_SubmitBtn();
		Thread.sleep(3000);
		if (getFormTitle().equalsIgnoreCase("Stay signed in?")) {
			click_SubmitBtn();
		}
		if (new elementVisible(driver).isAlertPresent()) {
			driver.switchTo().alert().accept();
			driver.switchTo().defaultContent();
		}
	}
}
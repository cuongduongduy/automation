package module;

import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class elementVisible {
	private WebDriverWait wait;
	private WebDriver driver;

	public elementVisible(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 30);
	}

	public WebElement isElementDisplay(WebElement element) {
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
		} catch (Exception e) {
			System.out.println("not found element: " +element);
			element = null;
		}
		return element;
	}
	
	public WebElement isElementClickAble(WebElement element) {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(element));
		} catch (Exception e) {
			System.out.println("not found element: "  +element);
			element = null;
		}
		return element;
	}

	public boolean checkElementPresentByXpath(String _xpath) {
		boolean present = false;
		try {
			driver.findElement(By.xpath(_xpath));
			present = true;
		} catch (NoSuchElementException e) {
			present = false;
		}
		return present;
	}

	public boolean isAlertPresent() {
		boolean foundAlert = false;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 15);
			wait.until(ExpectedConditions.alertIsPresent());
			foundAlert = true;
		} catch (NoAlertPresentException e) {
			foundAlert = false;
		}
		return foundAlert;
	}

}

package module;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import module.elementVisible;

public class pageHeader {
	private WebDriver driver;
	private WebElement pageTitle;

	public pageHeader(WebDriver driver) {
		this.driver = driver;
	}

	// Element list
	public String pageTitle() {
		pageTitle = new elementVisible(driver).isElementDisplay(driver.findElement(By.cssSelector(".page-title")));
		return pageTitle.getText();
	}

	// Functional list
	public boolean verifyPageTitle() {
		boolean verifyPageTitle = false;
		if (pageTitle().equalsIgnoreCase("Lloyds Banking Group MI Portal")) {
			verifyPageTitle = true;
		}
		return verifyPageTitle;
	}
}

package module;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPageTitle {
	private WebDriver driver;
	private WebDriverWait wait;
	private WebElement eTitle;

	private By by_pageTitle = By.xpath("//section[@class='content']/h2");

	public MainPageTitle(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement pageTitle() {
		try {
			wait = new WebDriverWait(driver, 30);
			wait.until(
					driver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete"));
			//wait.until(ExpectedConditions.visibilityOf(driver.findElement(by_pageTitle)));
			eTitle = driver.findElement(by_pageTitle);
		} catch (Exception e) {
			System.out.println("Element not found or time-out");
		}
		return eTitle;
	}

	public String getPageTitle() {
		return pageTitle().getText();
	}
}

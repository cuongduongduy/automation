package module;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import module.elementVisible;

public class exportData{
	private WebDriver driver;
	private WebElement btnExport;
	
	private elementVisible elementVisible;
	private By by_btnExport = By.cssSelector(".export-button");
	
	public exportData(WebDriver driver) {
		this.driver = driver;
		elementVisible = new elementVisible(driver);
	}
	
	public WebElement btnExport() {
		btnExport = elementVisible.isElementDisplay(driver.findElement(by_btnExport));
		return btnExport;
	}
	
	public boolean verifyTooltipExport() {
		boolean txtToolTip = false;
		if(btnExport().getAttribute("data-original-title").equalsIgnoreCase("Export Full Matter List")) {
			txtToolTip = true;
		}
		return txtToolTip;
	}
	
}

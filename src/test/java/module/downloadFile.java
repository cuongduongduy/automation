package module;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import LBGMIPortal.Automation.initBrowser;

public class downloadFile{
	private static WebDriverWait wait;
	public static String  fileName = null;
	
	public static File getLatestFilefromDir(String downloadFilepath){
	    File dir = new File(downloadFilepath);
	    File[] files = dir.listFiles();
	    if (files == null || files.length == 0) {
	        return null;
	    }
	
	    File lastModifiedFile = files[0];
	    for (int i = 0; i < files.length; i++) {
	       if (lastModifiedFile.lastModified() < files[i].lastModified()) {
	           lastModifiedFile = files[i];
	           System.out.println(lastModifiedFile.getName()+ " " +lastModifiedFile.lastModified());
	       }
	    }
	    return lastModifiedFile;
	}
	
	public static boolean VerifyExpectedFileName(WebDriver driver) throws InterruptedException {
		boolean verifyName = false;
		wait = new WebDriverWait(driver, 30);
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".export-button")));
		WebElement btnExport = driver.findElement(By.cssSelector(".export-button"));
		btnExport.click();
		Thread.sleep(10000);
		File getLatestFile;
		if(getLatestFilefromDir(initBrowser.downloadFilepath)!= null) {
			getLatestFile = getLatestFilefromDir(initBrowser.downloadFilepath);
			fileName = getLatestFile.getName();
			if(getLatestFile.getName().equalsIgnoreCase("LloydsBankingGroup_FullMatterList.xlsx")) {
				verifyName = true;
			}
			getLatestFile.delete();
		}
		else {
			System.out.println("Error! No file downloaded");
		}
		System.out.println(fileName);
		return verifyName;
	}
}
